#!/bin/bash

model_dir=`pwd`/expts/models
data_dir=`pwd`/expts/data
out_dir=`pwd`/expts/output
score_dir=`pwd`/expts/score
tmp_dir=`pwd`/expts/.tmp

mkdir -p $model_dir $tmp_dir $out_dir $score_dir

patch_size=256


train_base=0
if [ $train_base -eq 1 ]; then
    echo 'Start super-resolution training...'

    # input params
    hr_db=$data_dir/hr.image.list.csv   #HR images
    lr_db=$data_dir/lr.image.list.csv   #LR images
    learning_rate=0.001
    batch_size=4
    sample_interval=500
    epochs=6000

    # output params
    input_path=$model_dir
    output_path=$model_dir

    python -u scps/train.py --HR-image-db $hr_db --LR-image-db $lr_db \
                            --input-generator-path $model_dir/gen_model.h5 --input-discriminator-path $model_dir/dis_model.h5 \
                            --patch-size $patch_size --batch-size $batch_size --sample-interval $sample_interval \
                            --learning-rate $learning_rate --epochs $epochs \
                            --output-path $output_path || exit 1; 

fi

exit 0;
