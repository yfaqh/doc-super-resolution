import warnings

with warnings.catch_warnings():
    import keras
    import numpy as np
    import etl, loss
    import os
    from keras.layers.convolutional import (
        Conv2D,
        Conv2DTranspose,
        MaxPooling2D,
        Cropping2D,
    )
    from keras.layers.merge import add
    from keras.layers import (
        Input,
        Activation,
        LeakyReLU,
        Reshape,
        Permute,
        PReLU,
        Dot,
        concatenate,
        UpSampling2D,
        Lambda,
    )
    import keras
    from keras import backend as K
    from keras.layers.core import Dense, Flatten, Dropout
    from keras.engine.topology import Layer, get_source_inputs
    from keras.models import Model
    from keras.regularizers import l2
    from keras.layers.normalization import BatchNormalization
    from datetime import datetime


def _conv_bn_relu(**conv_params):
    """Helper to build a conv -> BN -> relu block
    """
    name = conv_params["name"]
    filters = conv_params["filters"]
    strides = conv_params.setdefault("strides", 1)
    padding = conv_params.setdefault("padding", "same")
    kernel_size = conv_params["kernel_size"]
    kernel_initializer = conv_params.setdefault("kernel_initializer", "he_normal")
    kernel_regularizer = conv_params.setdefault("kernel_regularizer", l2(1.0e-4))

    def f(input):
        conv = Conv2D(
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            padding=padding,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            name=name,
        )(input)
        norm = BatchNormalization(axis=-1)(conv)
        return LeakyReLU(alpha=0.3)(norm)

    return f


def _deconv_bn_relu(**conv_params):
    """Helper to buidl a deconv -> BN -> relu block
    """
    name = conv_params["name"]
    filters = conv_params["filters"]
    strides = conv_params.setdefault("strides", 1)
    padding = conv_params.setdefault("padding", "same")
    activation = conv_params.setdefault("activation", "relu")
    kernel_size = conv_params["kernel_size"]
    kernel_initializer = conv_params.setdefault("kernel_initializer", "he_normal")
    kernel_regularizer = conv_params.setdefault("kernel_regularizer", l2(1.0e-4))

    def f(input):
        # up = UpSampling2D(size=strides)(input)
        conv = Conv2DTranspose(
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            padding=padding,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            name=name,
        )(input)
        norm = BatchNormalization(axis=-1)(conv)
        return Activation(activation)(norm)

    return f


def _res_conv(**params):
    """Helper to build resnet
       refer: https://keunwoochoi.wordpress.com/2016/03/09/residual-networks-implementation-on-keras/
    """
    name = params["name"]
    filters = params["filters"]
    strides = params.setdefault("strides", 1)
    kernel_size = params["kernel_size"]

    def f(input):
        shortcut = Cropping2D(cropping=((0, 0), (0, 0)))(input)

        x = Conv2D(
            filters=filters, kernel_size=kernel_size, strides=(1, 1), padding="same"
        )(input)
        x = BatchNormalization(axis=-1)(x)
        x = PReLU(
            alpha_initializer="zeros",
            alpha_regularizer=None,
            alpha_constraint=None,
            shared_axes=[1, 2],
        )(x)
        x = Conv2D(
            filters=filters, kernel_size=kernel_size, strides=(1, 1), padding="same"
        )(x)
        x = BatchNormalization(axis=-1)(x)

        return add([shortcut, x], name=name)

    return f


def _attention(**params):
    name = params["name"]
    kernel_initializer = params.setdefault("kernel_initializer", "he_normal")
    kernel_regularizer = params.setdefault("kernel_regularizer", l2(1.0e-4))

    def hw_flatten(x):
        batch_size, height, width, num_channels = x.get_shape().as_list()
        return Reshape((height * width, num_channels))(x)

    def f(x):
        batch_size, height, width, num_channels = x.get_shape().as_list()

        q = BatchNormalization(axis=-1)(
            Conv2D(
                filters=num_channels // 8,
                kernel_size=3,
                strides=1,
                padding="same",
                kernel_initializer=kernel_initializer,
                kernel_regularizer=kernel_regularizer,
                use_bias=True,
            )(x)
        )  # [bs, h, w, c']

        k = BatchNormalization(axis=-1)(
            Conv2D(
                filters=num_channels // 8,
                kernel_size=3,
                strides=1,
                padding="same",
                kernel_initializer=kernel_initializer,
                kernel_regularizer=kernel_regularizer,
                use_bias=True,
            )(x)
        )  # [bs, h, w, c']

        v = BatchNormalization(axis=-1)(
            Conv2D(
                filters=num_channels,
                kernel_size=1,
                strides=1,
                padding="same",
                kernel_initializer=kernel_initializer,
                kernel_regularizer=kernel_regularizer,
                use_bias=True,
            )(x)
        )  # [bs, h, w, c']

        # N = h * wi
        s = Dot([2, 1])([hw_flatten(q), Permute([2, 1])(hw_flatten(k))])
        beta = Activation("softmax")(s)  # attention map

        o = Dot([2, 1])([beta, hw_flatten(v)])  # [bs, N, C]
        o = Reshape((height, width, num_channels))(o)  # [bs, h, w, C]

        o = BatchNormalization(axis=-1)(
            Conv2D(
                filters=num_channels,
                kernel_size=1,
                strides=1,
                padding="same",
                kernel_initializer=kernel_initializer,
                kernel_regularizer=None,
                use_bias=True,
            )(o)
        )  # [bs, h, w, c']

        x = add([o, x])

        return x

    return f


class Normalize(Layer):
    def __init__(self, **kwargs):
        super(Normalize, self).__init__(**kwargs)

    def build(self, input_shape):
        pass

    def compute_output_shape(self, input_shape):
        return input_shape

    def call(self, x, mask=None):
        return x / 255.0


class Denormalize(Layer):
    """
    Custom layer to denormalize the final Convolution layer activations (tanh)
    Since tanh scales the output to the range (-1, 1), we add 1 to bring it to the
    range (0, 2). We then multiply it by 127.5 to scale the values to the range (0, 255)
    """

    def __init__(self, **kwargs):
        super(Denormalize, self).__init__(**kwargs)

    def build(self, input_shape):
        pass

    def compute_output_shape(self, input_shape):
        return input_shape

    def call(self, x, mask=None):
        """
        Scales the tanh output activations from previous layer (-1, 1) to the
        range (0, 255)
        """
        return (x + 1) * 127.5


class Generator(object):
    def __init__(self, input_shape):
        self.input_shape = input_shape

    def generator(self):
        input_lr_images = Input(
            shape=(self.input_shape[0], self.input_shape[1], 1), name="input_lr_images"
        )

        inputs = Normalize()(input_lr_images)
        x = _conv_bn_relu(
            filters=32, kernel_size=(5, 5), strides=1, name="encoder_conv1"
        )(
            inputs
        )  # 256x256x32
        shortcut1 = x
        x = _conv_bn_relu(
            filters=64, kernel_size=(5, 5), strides=2, name="encoder_conv2"
        )(
            x
        )  # 128x128x64
        shortcut2 = x
        x = _conv_bn_relu(
            filters=128, kernel_size=(3, 3), strides=2, name="encoder_conv3"
        )(
            x
        )  # 64x64x128
        shortcut3 = x
        x = _conv_bn_relu(
            filters=256, kernel_size=(3, 3), strides=2, name="encoder_conv4"
        )(
            x
        )  # 32x32x256

        x = _res_conv(filters=256, kernel_size=(3, 3), name="encoder_embedding1")(x)
        x = _attention(name="encoder_attn1")(x)
        x = _res_conv(filters=256, kernel_size=(3, 3), name="decoder_embedding1")(x)
        x = _attention(name="decoer_attn1")(x)

        x = _deconv_bn_relu(
            filters=128, kernel_size=(3, 3), strides=2, name="decoder_deconv1"
        )(
            x
        )  # 64x64x128
        x = add([shortcut3, x])
        x = _deconv_bn_relu(
            filters=64, kernel_size=(3, 3), strides=2, name="decoder_deconv2"
        )(
            x
        )  # 128x128x64
        x = add([shortcut2, x])
        x = _deconv_bn_relu(
            filters=32, kernel_size=(3, 3), strides=2, name="decoder_deconv3"
        )(
            x
        )  # 256x256x32
        x = add([shortcut1, x])

        # Last 1x1 Conv2D
        x = Activation("tanh")(
            Conv2D(filters=1, kernel_size=(1, 1), strides=1, name="decoder_last_conv")(
                x
            )
        )  # 256x256x1

        # Scale output to range [0, 255] via custom Denormalize layer
        generated_images = Denormalize(name="decoder_outputs")(x)

        return Model(inputs=input_lr_images, outputs=generated_images, name="generator")


class Discriminator(object):
    def __init__(self, input_shape):
        self.input_shape = input_shape

    def discriminator(self):
        # take inputs from generator
        input_hr_images = Input(
            shape=(self.input_shape[0], self.input_shape[1], 1), name="input_hr_images"
        )

        inputs = Normalize()(input_hr_images)

        # resnet 1
        x = _conv_bn_relu(filters=32, kernel_size=(7, 7), name="conv1")(inputs)
        b = _conv_bn_relu(filters=16, kernel_size=(1, 1), name="res1_conv1")(x)
        b = _conv_bn_relu(filters=16, kernel_size=(3, 3), name="res1_conv2")(b)
        b = _conv_bn_relu(filters=32, kernel_size=(1, 1), name="res1_conv3")(b)
        m = LeakyReLU(alpha=0.2, name="Loss_Leaky1")(add([x, b]))

        # resnet 2
        x = MaxPooling2D((2, 2), padding="same")(m)  # 128x128
        x = _conv_bn_relu(filters=64, kernel_size=(3, 3), name="conv2")(x)
        b = _conv_bn_relu(filters=32, kernel_size=(1, 1), name="res2_conv1")(x)
        b = _conv_bn_relu(filters=32, kernel_size=(3, 3), name="res2_conv2")(b)
        b = _conv_bn_relu(filters=64, kernel_size=(1, 1), name="res2_conv3")(b)
        m = LeakyReLU(alpha=0.2, name="Loss_Leaky2")(add([x, b]))

        # resnet 3
        x = MaxPooling2D((2, 2), padding="same")(m)  # 64x64
        x = _conv_bn_relu(filters=128, kernel_size=(3, 3), name="conv3")(x)
        b = _conv_bn_relu(filters=64, kernel_size=(1, 1), name="res3_conv1")(x)
        b = _conv_bn_relu(filters=64, kernel_size=(3, 3), name="res3_conv2")(b)
        b = _conv_bn_relu(filters=128, kernel_size=(1, 1), name="res3_conv3")(b)
        m = LeakyReLU(alpha=0.2, name="Loss_Leaky3")(add([x, b]))

        # resnet 4
        x = MaxPooling2D((2, 2), padding="same")(m)  # 32x32
        x = _conv_bn_relu(filters=256, kernel_size=(3, 3), name="conv4")(x)
        b = _conv_bn_relu(filters=128, kernel_size=(1, 1), name="res4_conv1")(x)
        b = _conv_bn_relu(filters=128, kernel_size=(3, 3), name="res4_conv2")(b)
        b = _conv_bn_relu(filters=256, kernel_size=(1, 1), name="res4_conv3")(b)
        m = LeakyReLU(alpha=0.2, name="Loss_Leaky4")(add([x, b]))

        x = Flatten()(m)

        x = Dense(units=1024, activation="relu")(x)
        x = Dropout(rate=0.3)(x)
        x = Dense(units=512, activation="relu")(x)
        x = Dropout(rate=0.3)(x)
        x = Dense(units=1, activation="sigmoid")(x)

        return Model(inputs=input_hr_images, outputs=x, name="discriminator")


class SuperDocGAN(object):
    def __init__(
        self,
        hr_dataset_path,
        lr_dataset_path,
        in_generator_path,
        in_discriminator_path,
        input_shape,
        learning_rate,
    ):
        self.image_shape = input_shape
        self.misc_data_loader = etl.MiscDataLoader(
            hr_dataset_path, lr_dataset_path, input_shape
        )

        optimizer_g = keras.optimizers.SGD(
            lr=learning_rate, momentum=0.9, decay=1e-06, nesterov=True, clipnorm=5
        )
        optimizer_d = keras.optimizers.Adam(
            lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08
        )

        # build generator
        self.generator = Generator(input_shape).generator()
        if os.path.isfile(in_generator_path):
            self.generator.load_weights(in_generator_path)

        # build discriminator
        self.discriminator = Discriminator(input_shape).discriminator()
        if os.path.isfile(in_discriminator_path):
            self.discriminator.load_weights(in_discriminator_path)

        # build gan (generator_train & disrciminator_train)
        input_hr_image = Input(shape=(input_shape[0], input_shape[1], 1))
        input_lr_image = Input(shape=(input_shape[0], input_shape[1], 1))
        generated_image = self.generator(input_lr_image)

        # relative confidence
        output_fake_conf = self.discriminator(generated_image)
        output_real_conf = self.discriminator(input_hr_image)

        real_fake_relativistic_average_conf = Lambda(
            loss.relativistic_average, name="real_minus_mean_fake_conf"
        )([output_real_conf, output_fake_conf])
        fake_real_relativistic_average_conf = Lambda(
            loss.relativistic_average, name="fake_minus_mean_real_conf"
        )([output_fake_conf, output_real_conf])

        real_fake_relativistic_average_conf = Activation("sigmoid")(
            real_fake_relativistic_average_conf
        )
        fake_real_relativistic_average_conf = Activation("sigmoid")(
            fake_real_relativistic_average_conf
        )

        discriminator_relativistic_conf_out = concatenate(
            [real_fake_relativistic_average_conf, fake_real_relativistic_average_conf]
        )

        # generator_train
        self.generator_train = Model(
            [input_lr_image, input_hr_image], discriminator_relativistic_conf_out
        )
        self.discriminator.trainable = False
        self.generator_train.compile(loss=["mse"], optimizer=optimizer_g)

        # discriminator_train
        self.discriminator_train = Model(
            [input_lr_image, input_hr_image], discriminator_relativistic_conf_out
        )
        self.generator.trainable = False
        self.discriminator.trainable = True
        self.discriminator_train.compile(loss=["mse"], optimizer=optimizer_d)

    def train(self, epochs, batch_size, sample_interval=50, output_dir=None):
        start_time = datetime.now()

        d_loss_record = np.inf
        g_loss_record = np.inf

        for epoch in range(epochs):
            # target labels
            real_y = (
                np.ones((batch_size, 1))
                - np.random.random_sample((batch_size, 1)) * 0.01
            )
            fake_y = (
                -np.ones((batch_size, 1))
                + np.random.random_sample((batch_size, 1)) * 0.01
            )
            y_for_gen = np.concatenate((fake_y, real_y), axis=1)
            y_for_dis = np.concatenate((real_y, fake_y), axis=1)

            # ---------------------
            # train discriminator
            # ---------------------
            # sample images and their conditioning counterparts
            imgs_hr, imgs_lr = self.misc_data_loader.load_data(batch_size)

            self.discriminator.trainable = True
            self.generator.trainable = False
            d_loss = self.discriminator_train.train_on_batch(
                [imgs_lr, imgs_hr], y_for_dis
            )

            # ---------------------
            # train generator
            # ---------------------
            imgs_hr, imgs_lr = self.misc_data_loader.load_data(batch_size)

            # The generators want the discriminators to label the generated images as real
            self.discriminator.trainable = False
            self.generator.trainable = True
            g_loss = 0
            for i in range(2):
                g_loss += self.generator_train.train_on_batch(
                    [imgs_lr, imgs_hr], y_for_gen
                )
            g_loss /= 2

            if g_loss < g_loss_record:
                self.generator.save(f"{output_dir}/gen_model.h5")
                g_loss_record = g_loss

            if d_loss < d_loss_record:
                self.discriminator.save(f"{output_dir}/dis_model.h5")
                d_loss_record = d_loss

            elapsed_time = datetime.now() - start_time
            print(
                "%d time: %s \t Discriminator loss: %f \t GAN loss: %f"
                % (epoch + 1, elapsed_time, d_loss, g_loss)
            )
