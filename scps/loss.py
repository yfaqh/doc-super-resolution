import warnings

with warnings.catch_warnings():
    import numpy as np
    import tensorflow as tf
    from keras import backend as K
    from keras.applications.vgg19 import VGG19
    from keras.regularizers import Regularizer
    from keras.models import Model


def relativistic_average(input_):
    d = input_[0] - K.mean(input_[1], axis=0)
    return d 

