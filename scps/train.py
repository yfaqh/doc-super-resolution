import warnings

with warnings.catch_warnings():
    import os, sys
    import argparse
    import keras
    import nets
    from keras.callbacks import ModelCheckpoint
    from datetime import datetime


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = argparse.ArgumentParser(
        description="document image super-resolution training."
    )
    parser.add_argument(
        "--HR-image-db", dest="hr_db", help="The input HR image training dataset"
    )
    parser.add_argument(
        "--LR-image-db", dest="lr_db", help="The input LR image training dataset"
    )
    parser.add_argument("--sample-interval", type=int, dest="sample_interval")
    parser.add_argument(
        "-s",
        "--patch-size",
        type=int,
        dest="patch_size",
        help="The croped patches' dimension",
    )
    parser.add_argument(
        "-b",
        "--batch-size",
        type=int,
        dest="batch_size",
        help="The batch size for  training",
    )
    parser.add_argument(
        "-r", "--learning-rate", type=float, dest="lr", help="The learning rate"
    )
    parser.add_argument(
        "-e", "--epochs", type=int, dest="epochs", help="Training epochs"
    )
    parser.add_argument(
        "-g",
        "--input-generator-path",
        dest="in_generator",
        default="",
        help="Input generator model path",
    )
    parser.add_argument(
        "-d",
        "--input-discriminator-path",
        dest="in_discriminator",
        default="",
        help="Input discriminator model path",
    )
    parser.add_argument(
        "-o", "--output-path", dest="output_dir", help="Output model path"
    )
    args = parser.parse_args()

    gan = nets.SuperDocGAN(
        args.hr_db,
        args.lr_db,
        args.in_generator,
        args.in_discriminator,
        (args.patch_size, args.patch_size),
        args.lr,
    )

    gan.train(args.epochs, args.batch_size, args.sample_interval, args.output_dir)


if __name__ == "__main__":
    sys.exit(main())
